-- SUMMARY --

This module can be used to count the number of lines and characters contained
in the files of a project. It can traverse a given project directory recursively
 and process files that match a given file name pattern. The module counts the
number of lines of the matched files and the total number characters excluding
white space or just considering alphabetic characters.

The module can also display the statistics in a Web page.

-- INSTALLATION --

See the installation guide here - http://drupal.org/node/70151
